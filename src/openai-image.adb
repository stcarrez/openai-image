with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Exceptions;
with Ada.Streams.Stream_IO;
with Ada.IO_Exceptions;
with Ada.Strings.Unbounded;

with GNAT.Command_Line;

with Util.Http.Clients.Curl;
with Util.Http.Clients;
with Util.Strings;
with Util.Properties;
with Util.Log.Loggers;
with Util.Streams.Files;

with OpenAPI;
with OpenAPI.Credentials.OAuth;

with OpenAI.Clients;
with OpenAI.Models;
procedure OpenAI.Image is

   use Ada.Text_IO;
   use Ada.Strings.Unbounded;
   use GNAT.Command_Line;

   procedure Usage;
   procedure Download_Image (Uri : String; Name : String);
   function Get_Api_Key return String;
   procedure Initialize;

   Error : exception;

   Log       : constant Util.Log.Loggers.Logger
     := Util.Log.Loggers.Create ("OpenAI.Image");
   Server    : constant OpenAPI.Ustring
     := OpenAPI.To_UString ("https://api.openai.com/v1");
   Arg_Count : constant Natural := Ada.Command_Line.Argument_Count;
   Config    : Util.Properties.Manager;

   procedure Usage is
   begin
      Put_Line ("Usage: openai-image [-v] [-s] [-m] [-l] [-c count] "
                  & "[-p prefix] {params}...");
      Put_Line ("-v        Verbose execution");
      Put_Line ("-s        Small image generation (256x256)");
      Put_Line ("-m        Medium image generation (512x512)");
      Put_Line ("-l        Large image generation (1024x1024)");
      Put_Line ("-c count  Generate `count` images (1..10)");
      Put_Line ("-p prefix The file name prefix to save the "
                  & "image (<prefix>-<index>.png)");
      Put_Line ("params    Textual description of the image to generate");
   end Usage;

   procedure Download_Image (Uri : String; Name : String) is
      Client   : Util.Http.Clients.Client;
      Response : Util.Http.Clients.Response;
   begin
      Log.Info ("Download image: {0}", Uri);

      Client.Add_Header ("X-Requested-By", "wget");
      Client.Get (Uri, Response);
      if Response.Get_Status = 200 then
         declare
            Out_Stream : aliased Util.Streams.Files.File_Stream;
            Data       : constant OpenAPI.Blob_Ref := Response.Get_Body;
         begin
            Log.Info ("Saving image in {0}", Name);
            Out_Stream.Create (Mode => Ada.Streams.Stream_IO.Out_File,
                               Name => Name);
            Out_Stream.Write (Data.Value.Data);
         end;
      else
         Log.Error ("Error {0} when downloading image",
                    Util.Strings.Image (Response.Get_Status));
      end if;
   end Download_Image;

   function Get_Api_Key return String is
      Config    : Util.Properties.Manager;
   begin
      Config.Load_Properties ("openai-key.properties");
      return Config.Get ("openai_key");

   exception
      when E : Ada.Io_Exceptions.Name_Error =>
         Log.Error ("No API key: " & Ada.Exceptions.Exception_Message (E));
         raise Error;

   end Get_Api_Key;

   procedure Initialize is
   begin
      begin
         Config.Load_Properties ("openai.properties");

      exception
         when Ada.Io_Exceptions.Name_Error =>
            null;
      end;
      Util.Log.Loggers.Initialize (Config);
   end Initialize;

   subtype Count_Type is Integer range 1 .. 10;

   Image_Size  : OpenAPI.UString := OpenAPI.To_UString ("256x256");
   File_Prefix : OpenAPI.Ustring := OpenAPI.To_UString ("image-");
   Count       : Count_Type := 1;
   First       : Natural := 0;
begin
   Initialize;
   Initialize_Option_Scan (Stop_At_First_Non_Switch => True);
   loop
      case Getopt ("* v s l m c: p:") is
         when ASCII.NUL =>
            exit;

         when 'v' =>
            Config.Set ("log4j.logger.OpenAI.Image", "INFO");
            Config.Set ("log4j.appender.console.level", "INFO");
            Util.Log.Loggers.Initialize (Config);

         when 's' =>
            Image_Size := OpenAPI.To_Ustring ("256x256");

         when 'l' =>
            Image_Size := OpenAPI.To_Ustring ("512x512");

         when 'm' =>
            Image_Size := OpenAPI.To_Ustring ("1024x1024");

         when 'p' =>
            File_Prefix := OpenAPI.To_Ustring (Parameter);
            First := First + 1;

         when 'c' =>
            begin
               Count := Count_Type'Value (Parameter);
            exception
               when others =>
                  Usage;
            end;
            First := First + 1;

         when '*' =>
            exit;

         when others =>
            null;
      end case;
      First := First + 1;
   end loop;
   First := First + 1;
   if First >= Arg_Count then
      Usage;
      return;
   end if;
   Util.Http.Clients.Curl.Register;
   declare
      Api_Key : constant String := Get_Api_Key;
      Prompt  : OpenAPI.UString;
      Cred    : aliased OpenAPI.Credentials.OAuth.OAuth2_Credential_Type;
      C       : OpenAI.Clients.Client_Type;
      Req     : OpenAI.Models.CreateImagesRequest_Type;
      Reply   : OpenAI.Models.ImagesResponse_Type;
      Index   : Natural := 0;
   begin
      for I in First .. Arg_Count loop
         if I /= First then
            Append (Prompt, " ");
         end if;
         Append (Prompt, Ada.Command_Line.Argument (I));
      end loop;

      Log.Info ("Create image '{0}'", Prompt);
      Cred.Bearer_Token (Api_Key);
      C.Set_Server (Server);
      C.Set_Credentials (Cred'Unchecked_Access);

      Req.Prompt := Prompt;
      Req.N := (Is_Null => False, Value => Count);
      Req.Size := (Is_Null => False, Value => Image_Size);
      C.Create_Image (Req, Reply);

      for Url of Reply.Data loop
         if not Url.Url.Is_Null then
            Index := Index + 1;
            Download_Image (OpenAPI.To_String (Url.Url.Value),
                            OpenAPI.To_String (File_Prefix)
                              & Util.Strings.Image (Index) & ".png");
         end if;
      end loop;

   exception
      when E : Constraint_Error =>
         Put_Line ("Constraint error raised: "
                     & Ada.Exceptions.Exception_Message (E));

   end;

exception
   when Error =>
      Ada.Command_Line.Set_Exit_Status (1);

end OpenAI.Image;
