# Ada OpenAI Image Generator

Image generation example with [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai) by using
[OpenAI](https://openai.com/) [DALL.E](https://platform.openai.com/docs/models/dall-e) model.

## Setup and build

Use Alire to setup and build the project:

```
git clone https://gitlab.com/stcarrez/openai-image.git
cd openai-image
alr build
```

## OpenAI Access Key

The [OpenAI](https://openai.com/) service uses an OAuth bearer API key
to authenticate requests made on the server.  For the credential setup you will first need to get your access key
from your [OpenAI account](https://platform.openai.com/login).
Once you have your key, create the file `openai-key.properties` file with the command:

```
echo "openai_key=XXXXX" > openai-key.properties
```

## Creating an image

After building and getting the `openai-key.properties` file setup, launch the generation tool and
give a description of the image you want to generate:

```
./bin/openai-image -v realistic photo of Ada Lovelace programming a modern computer
```

## API for image creation

The image generation is provided by calling the `Create_Image` operation and filling a
`CreateImagesRequest_Type` object with the requery.  The operation will make an HTTP POST
request with the query and it returns the results in an `ImagesResponse_Type` object.

```
C       : OpenAI.Clients.Client_Type;
Req     : OpenAI.Models.CreateImagesRequest_Type;
Reply   : OpenAI.Models.ImagesResponse_Type;
...
  Req.Prompt := OpenAPI.To_UString ("imagine an artificial intelligence brain that solves image generation problems");
  Req.N := (Is_Null => False, Value => 3);
  Req.Size := (Is_Null => False, Value => OpenAPI.To_UString ("256x256"));
  C.Create_Image (Req, Reply);
```

Upon successful completion, we get a list of URLs that give access to the generated images.
These URLs are valid for some times (3 to 4 weeks?) and will expire.  You can iterate over
the list with the following code extract:

```
  for Url of Reply.Data loop
     if not Url.Url.Is_Null then
        --  Download image at following URL
        Ada.Text_IO.Put_Line (OpenAPI.To_String (Url.Url.Value));
     end if;
  end loop;
```

## Links

- [OpenAI API reference](https://platform.openai.com/docs/api-reference)
- [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai)

